/* Теоретичні питання
1. Опишіть своїми словами як працює метод forEach. - перебирає елементи в масиві
2. Як очистити масив? - arr.length = 0
3. Як можна перевірити, що та чи інша змінна є масивом? - Array.isArray(variable)
 */


function filterBy(arr, typeOf){
  const  newArr = arr.filter((element) => typeof element !== typeOf)
    return newArr;
}

console.log(filterBy(['hello', 'world', 23, true, '23', null, [1, 2, 3], {},], 'string'));


